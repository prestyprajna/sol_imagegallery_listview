﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ImageGallery_Using_ListView.Models
{
    public class ImageEntity
    {
        public decimal? ImageId { get; set; }

        public string ImageName { get; set; }

        public string VirtualPath { get; set; }
    }
}