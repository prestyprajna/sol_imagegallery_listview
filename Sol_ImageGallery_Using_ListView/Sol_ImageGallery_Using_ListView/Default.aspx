﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="Default.aspx.cs" Inherits="Sol_ImageGallery_Using_ListView.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">

        li{
            display:inline;
            float:left;
            margin-left:15px;
            margin-bottom:15px;
        }

    </style>

     <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

</head>
<body>
    <form id="form1" runat="server">
    <div>

         <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <asp:ListView ID="lvImage" runat="server" DataKeyNames="ImageId" ItemType="Sol_ImageGallery_Using_ListView.Models.ImageEntity" SelectMethod="BindData">

                    <LayoutTemplate>
                        <ul >

                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>

                        </ul>

                        <div runat="server">
                            <asp:DataPager ID="dp" runat="server" PageSize="2" PagedControlID="lvImage">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                        ShowNextPageButton="false" ButtonCssClass="w3-bar-item w3-button" />
                                    <asp:NumericPagerField ButtonType="Link" NumericButtonCssClass="w3-button" CurrentPageLabelCssClass="w3-button" NextPreviousButtonCssClass="w3-button" />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" ButtonCssClass="w3-bar-item w3-button" />
                                </Fields>
                            </asp:DataPager>
                        </div>

                    </LayoutTemplate>

                    <ItemTemplate>

                        <li>
                            <div class="w3-card-4 w3-blue">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgCakes" runat="server" ImageUrl='<%# BindItem.VirtualPath %>' />

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCakesNames" runat="server" Text='<%#BindItem.ImageName %>'></asp:Label>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>    

                    </ItemTemplate>

                    <EditItemTemplate>

                    </EditItemTemplate>

                    <EmptyDataTemplate>

                    </EmptyDataTemplate>

                </asp:ListView>


            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
