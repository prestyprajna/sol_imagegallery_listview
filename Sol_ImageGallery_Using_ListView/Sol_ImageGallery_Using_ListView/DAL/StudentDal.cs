﻿using Sol_ImageGallery_Using_ListView.DAL.ORD;
using Sol_ImageGallery_Using_ListView.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ImageGallery_Using_ListView.DAL
{
    public class StudentDal
    {
        #region  declaration
        private ImageDCDataContext _dc = null;
        #endregion
         
        #region  constructor
        public StudentDal()
        {
            _dc = new ImageDCDataContext();
        }

        #endregion

        #region  public methods

        private async Task<IEnumerable<ImageEntity>> GetImageData()
        {
            return await Task.Run(() =>
            {
                var getQuery =
                _dc?.tblUserImages
                ?.AsEnumerable()
                ?.Select((tblUserImageObj) => new ImageEntity()
                {
                    ImageId = tblUserImageObj?.ImageId,
                    ImageName = tblUserImageObj?.ImageName,
                    VirtualPath = tblUserImageObj?.VirtualPath
                })
                ?.ToList();

                return getQuery;


            });
        }

        public async Task<int?> GetImageCount()
        {
            return await Task.Run(async() =>
            {
                var getCount =
                (await this.GetImageData())
                ?.AsEnumerable()
                ?.Count();

                return getCount;
            });
        }

        public async Task<IEnumerable<ImageEntity>> GetImagePagination(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async () =>
            {
                var getCount =
                (await this.GetImageData())
                ?.AsEnumerable()
                ?.Skip(startRowIndex)
                ?.Take(maximumRows);
               

                return getCount;
            });
        }

        #endregion
    }
}