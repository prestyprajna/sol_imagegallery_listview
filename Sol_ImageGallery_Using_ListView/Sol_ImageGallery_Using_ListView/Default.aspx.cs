﻿using Sol_ImageGallery_Using_ListView.DAL;
using Sol_ImageGallery_Using_ListView.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ImageGallery_Using_ListView
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        //public IQueryable<Sol_ImageGallery_Using_ListView.Models.ImageEntity> lvImage_GetData()
        //{
        //    return null;
        //}

        public async Task<SelectResult> BindData(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async() =>
            {
                int count = Convert.ToInt32(await new StudentDal().GetImageCount());

                IEnumerable<ImageEntity> imageObj = await new StudentDal().GetImagePagination(startRowIndex, maximumRows);

                return new SelectResult(count, imageObj);
            });

        }
    }
}