﻿CREATE TABLE [dbo].[tblAccount] (
    [AccountId]       NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [TransactionDate] DATETIME     NULL,
    [DebitAmt]        NUMERIC (18) NULL,
    [CreditAmt]       NUMERIC (18) NULL,
    [BalanceAmt]      NUMERIC (18) NULL,
    PRIMARY KEY CLUSTERED ([AccountId] ASC)
);

