﻿CREATE TABLE [dbo].[tblUsers] (
    [StudentId] NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NOT NULL,
    [LastName]  VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([StudentId] ASC)
);

