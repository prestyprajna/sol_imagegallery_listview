﻿CREATE TABLE [dbo].[tblUserImage]
(
	[ImageId] NUMERIC NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [ImageName] VARCHAR(50) NULL, 
    [VirtualPath] VARCHAR(MAX) NULL
)
