﻿CREATE TABLE [dbo].[tblTempUser] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [Age]       INT          NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

