﻿CREATE PROCEDURE uspSetAccount
(	
	@TransactionDate DATETIME,	
	@Debit NUMERIC(18,0),
	@Credit NUMERIC(18,0)

	--@Balance NUMERIC(18,0) 
)
AS
	BEGIN	

	--DECLARATION
	DECLARE @tempBalance NUMERIC(18,0)=0

	DECLARE @LastIndex NUMERIC(18,0)=@@IDENTITY

	SELECT @tempBalance=A.BalanceAmt
	FROM tblAccount AS A
		WHERE A.AccountId=@LastIndex

		IF @Debit IS NOT NULL
			BEGIN 				
				SET @tempBalance =@tempBalance+@Debit
			END
		ELSE IF @Credit IS NOT NULL
			BEGIN
				SET @tempBalance=@tempBalance-@Credit
			END		

		INSERT INTO tblAccount
		(
			TransactionDate,
			CreditAmt,
			DebitAmt,
			BalanceAmt
		)
		VALUES
		(
			@TransactionDate,			
			@Credit,
			@Debit,
			@tempBalance
		)

		--SELECT A.TransactionDate,
		--A.DebitAmt,
		--A.CreditAmt,
		--A.BalanceAmt
		--	FROM tblAccount AS A	

	END
